import React, { useState } from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as Hamburger } from "../../assets/Hamburger.svg";
import Quantiphi from "../../assets/Quantiphi.png";

import "./SideNavBar.styles.scss";

const SidebarUI = ({ isOpen, ...rest }) => {
	const classes = ["Sidebar", isOpen ? "is-open" : ""];

	return (
		<div aria-hidden={!isOpen} className={classes.join(" ")} {...rest} />
	);
};

SidebarUI.Overlay = (props) => <div className="SidebarOverlay" {...props} />;

SidebarUI.Content = ({ width = "20rem", isRight = false, ...rest }) => {
	const classes = ["SidebarContent", isRight ? "is-right" : ""];
	const style = {
		width,
		height: "100%",
		top: 0,
		right: isRight ? `-${width}` : "auto",
		left: !isRight ? `-${width}` : "auto",
	};

	return <div className={classes.join(" ")} style={style} {...rest} />;
};

const SideNavBar = (props) => {
	const [isOpen, setIsOpen] = useState(false);

	function openSidebar(isOp = true) {
		setIsOpen(isOp);
	}

	const { hasOverlay, isRight } = props;

	return (
		<SidebarUI isOpen={isOpen}>
			<Hamburger onClick={openSidebar} className="ham" />

			<SidebarUI.Content
				isRight={isRight}
				onClick={() => openSidebar(false)}
			>
				<div className="content-logo">
					<NavLink to="/">
						<img src={Quantiphi} className="logo-image" />
					</NavLink>
				</div>
				<div className="content-inner">
					<div className="side-bar-tabs">
						<NavLink
							activeClassName="active"
							className="link nav_link"
							to="/questions"
						>
							<p>All Questions</p>
						</NavLink>
						<NavLink
							activeClassName="active"
							className="link nav_link"
							to="/tags"
						>
							<p>Tags</p>
						</NavLink>
						<NavLink
							activeClassName="active"
							className="link nav_link"
							to="/users"
						>
							<p>Users</p>
						</NavLink>
					</div>
				</div>
			</SidebarUI.Content>

			{hasOverlay ? (
				<SidebarUI.Overlay onClick={() => openSidebar(false)} />
			) : (
				false
			)}
		</SidebarUI>
	);
};

export default SideNavBar;
