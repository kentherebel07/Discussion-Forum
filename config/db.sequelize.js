const Sequelize = require('sequelize');
const dotenv = require('dotenv');

dotenv.config();

const sequelize = new Sequelize("heroku_ac792b5c4139455", "b8c7200f83c1ea", "438a3202",
  {
    dialect: 'mysql',
    host: "us-cdbr-east-04.cleardb.com",
    define: {
      timestamps: false,
    },
  });

module.exports = sequelize;
